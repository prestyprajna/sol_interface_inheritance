﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Interface_Inheritance
{
    class Program
    {
        static void Main(string[] args)
        {
            //IA iaObj = new IA();  //cannot create an instance of abstract class or interface

            IA aObj = new A();
            aObj.Insert();
            aObj.Update();

            //IInsert insertObj = new B();    //bad practise
            //insertObj.Insert();

            //IDelete deleteObj = new B();
            //deleteObj.Delete();
        }
    }

    public interface IInsert
    {
        void Insert();
    }

    public interface IUpdate
    {
        void Update();

    }

    public interface IDelete
    {
        void Delete();
    }

    public interface IA:IInsert,IUpdate
    {

    }

    public class A : IA
    {
        public void Insert()
        {
            throw new NotImplementedException();
        }

        public void Update()
        {
            throw new NotImplementedException();
        }
    }

    //public class B : IInsert, IDelete  //bad practise
    //{
    //    public void Delete()
    //    {
    //        throw new NotImplementedException();
    //    }

    //    public void Insert()
    //    {
    //        throw new NotImplementedException();
    //    }
    //}
}
